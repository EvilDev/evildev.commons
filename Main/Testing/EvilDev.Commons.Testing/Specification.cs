﻿using System;
using System.Linq;

namespace EvilDev.Commons.Testing
{
    public abstract class Specification<TSut> : Specification<TSut, TSut>
            where TSut : class
    {
    }

    public abstract class Specification<TSut, TContract>
        where TSut : class, TContract
        where TContract : class
    {
        protected TContract Sut { get; private set; }

        protected Specification()
        {
            GivenThat();
            Sut = CreateSut();
            AndGivenThatAfterCreated();

            if (!GetType().GetCustomAttributes(typeof (ConstructorSpecificationAttribute), true).Any())
            {
                WhenIRun();
            }
        }

        protected virtual TContract CreateSut()
        {
            return (TContract)Activator.CreateInstance(typeof(TSut));
        }

        public virtual void GivenThat()
        {
        }

        public virtual void AndGivenThatAfterCreated()
        {
        }

        public virtual void WhenIRun()
        {
            throw new NotImplementedException("You must implement WhenIRun unless you add the ConstructorSpecification attribute");
        }
    }
}
