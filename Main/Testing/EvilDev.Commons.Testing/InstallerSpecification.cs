﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace EvilDev.Commons.Testing
{
    public abstract class InstallerSpecification<TSut>
        : Specification<TSut, IWindsorInstaller>
        where TSut : class, IWindsorInstaller
    {
        public IWindsorContainer Container { get; private set; }

        public override void GivenThat()
        {
            base.GivenThat();

            Container = new WindsorContainer();
        }

        public override void WhenIRun()
        {
            Sut.Install(Container, null);
        }
    }
}