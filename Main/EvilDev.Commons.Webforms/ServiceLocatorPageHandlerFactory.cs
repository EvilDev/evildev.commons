﻿using System;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using Microsoft.Practices.ServiceLocation;

namespace EvilDev.Commons.Webforms
{
    public class ServiceLocatorPageHandlerFactory
        : PageHandlerFactory
    {
        private static object GetInstance(Type type)
        {
            return ServiceLocator.Current.GetInstance(type);
        }

        public override IHttpHandler GetHandler(HttpContext context,
            string requestType, string virtualPath, string path)
        {
            var handler = base.GetHandler(context, requestType,
                virtualPath, path);

            if (handler != null)
            {
                InitializeInstance(handler);
                HookChildControlInitialization(handler);
            }

            return handler;
        }

        private void HookChildControlInitialization(object handler)
        {
            Page page = handler as Page;

            if (page != null)
            {
                // Child controls are not created at this point.
                // They will be when PreInit fires.
                page.PreInit += (s, e) => InitializeChildControls(page);
            }
        }

        private static void InitializeChildControls(Control contrl)
        {
            var childControls = GetChildControls(contrl);

            foreach (var childControl in childControls)
            {
                InitializeInstance(childControl);
                InitializeChildControls(childControl);
            }
        }

        private static Control[] GetChildControls(Control ctrl)
        {
            var flags =
                BindingFlags.Instance | BindingFlags.NonPublic;

            return (
                from field in ctrl.GetType().GetFields(flags)
                let type = field.FieldType
                where typeof(UserControl).IsAssignableFrom(type)
                let userControl = field.GetValue(ctrl) as Control
                where userControl != null
                select userControl).ToArray();
        }

        private static void InitializeInstance(object instance)
        {
            Type pageType = instance.GetType().BaseType;

            var ctor = GetInjectableConstructor(pageType);

            if (ctor != null)
            {
                try
                {
                    var args = GetMethodArguments(ctor);

                    ctor.Invoke(instance, args);
                }
                catch (Exception ex)
                {
                    var msg = string.Format("The type {0} " +
                        "could not be initialized. {1}", pageType,
                        ex.Message);

                    throw new InvalidOperationException(msg, ex);
                }
            }
        }

        private static ConstructorInfo GetInjectableConstructor(
            Type type)
        {
            return type.GetConstructors().OrderByDescending(c => c.GetParameters().Count()).First();
        }

        private static object[] GetMethodArguments(MethodBase method)
        {
            return (
                from parameter in method.GetParameters()
                let parameterType = parameter.ParameterType
                select GetInstance(parameterType)).ToArray();
        }
    }
}
