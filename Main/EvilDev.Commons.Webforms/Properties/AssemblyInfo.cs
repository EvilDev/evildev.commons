﻿using System.Reflection;
using EvilDev.Commons.Attributes;

[assembly: AssemblyTitle("EvilDev.Commons.Webforms")]
[assembly: AssemblyDescription("Evil Dev's personal collection of useful utilities and extensions.")]
[assembly: AssemblySummary("Evil Dev's personal collection of useful utilities and extensions.")]
[assembly: AssemblyCompany("Deville Solutions")]
[assembly: AssemblyProduct("EvilDev.Commons")]
[assembly: AssemblyCopyright("Copyright © Deville Solutions 2012")]
[assembly: AssemblyProjectUrl("http://code.evildev.net/evildev.commons/wiki/Home")]
[assembly: AssemblyIconUrl("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2012/Dec/06/evildev.commons-logo-3272278768-2_avatar.png")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyLicenseUrl("http://mit-license.org/")]