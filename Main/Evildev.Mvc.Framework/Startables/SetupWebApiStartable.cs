﻿using System.Web.Http;
using System.Web.Http.Dependencies;
using Castle.Core;

namespace Evildev.Mvc.Framework.Startables
{
    public class SetupWebApiStartable : IStartable
    {
        private readonly HttpConfiguration _config;
        private readonly IDependencyResolver _dependencyResolver;

        public SetupWebApiStartable(HttpConfiguration config, IDependencyResolver dependencyResolver)
        {
            _config = config;
            _dependencyResolver = dependencyResolver;
        }

        public void Start()
        {
            _config.DependencyResolver = _dependencyResolver;
        }

        public void Stop()
        {
        }
    }
}