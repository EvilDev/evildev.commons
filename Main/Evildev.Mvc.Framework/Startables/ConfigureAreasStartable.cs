﻿using System.Web.Mvc;
using Castle.Core;

namespace Evildev.Mvc.Framework.Startables
{
    public class ConfigureAreasStartable : IStartable
    {
        public void Start()
        {
            AreaRegistration.RegisterAllAreas();
        }

        public void Stop()
        {
        }
    }
}