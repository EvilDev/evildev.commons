﻿using Castle.Core;
using Microsoft.Practices.ServiceLocation;

namespace Evildev.Mvc.Framework.Startables
{
    public class ConfigureServiceLocatorStartable : IStartable
    {
        private readonly IServiceLocator _serviceLocator;

        public ConfigureServiceLocatorStartable(IServiceLocator serviceLocator)
        {
            _serviceLocator = serviceLocator;
        }

        public void Start()
        {
            ServiceLocator.SetLocatorProvider(() => _serviceLocator);
        }

        public void Stop()
        {
        }
    }
}