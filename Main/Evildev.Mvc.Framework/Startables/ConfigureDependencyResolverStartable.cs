﻿using System.Web.Mvc;
using Castle.Core;

namespace Evildev.Mvc.Framework.Startables
{
    public class ConfigureDependencyResolverStartable : IStartable
    {
        private readonly IDependencyResolver _resolver;

        public ConfigureDependencyResolverStartable(IDependencyResolver resolver)
        {
            _resolver = resolver;
        }

        public void Start()
        {
            DependencyResolver.SetResolver(_resolver);
        }

        public void Stop()
        {
        }
    }
}