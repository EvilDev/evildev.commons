﻿using System.IO;
using System.Reflection;
using Castle.Facilities.Startable;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using EvilDev.Commons;

namespace Evildev.Mvc.Framework
{
    public class MvcBootstrapper : IBootstrapper
    {
        private readonly IWindsorContainer _container;

        public MvcBootstrapper(IWindsorContainer container)
        {
            _container = container;
        }

        public void Dispose()
        {
            _container.Dispose();
        }

        public IBootstrapper Run()
        {
            _container.AddFacility<StartableFacility>();
            _container.AddFacility<TypedFactoryFacility>();
            _container.Install(FromAssembly.This());

            var callingAssembly = Assembly.GetCallingAssembly();
            var app = callingAssembly.FullName.Split('.')[0];
            var assemblyDir = Path.GetDirectoryName(callingAssembly.Location);
            _container.Install(FromAssembly.InDirectory(new AssemblyFilter(assemblyDir, string.Format("{0}*", app))));

            return this;
        }
    }
}