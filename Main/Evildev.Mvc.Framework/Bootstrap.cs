﻿using Castle.Windsor;
using EvilDev.Commons;

namespace Evildev.Mvc.Framework
{
    public static class Bootstrap
    {
         public static IBootstrapper Mvc()
         {
             return new MvcBootstrapper(new WindsorContainer());
         }
    }
}