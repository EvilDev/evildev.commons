﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Evildev.Commons.Windsor.Mvc;

namespace Evildev.Mvc.Framework.Installers
{
    public class MvcInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<HttpConfiguration>().Instance(GlobalConfiguration.Configuration));
            container.Register(Component.For<GlobalFilterCollection>().Instance(GlobalFilters.Filters));
            container.Register(Component.For<RouteCollection>().Instance(RouteTable.Routes));
            container.Register(Component.For<ControllerBuilder>().Instance(ControllerBuilder.Current));
            container.Register(Component.For<IControllerFactory>().ImplementedBy<WindsorControllerFactory>());
            container.Register(Component.For<IDependencyResolver>().ImplementedBy<WindsorDependencyResolver>());
        }
    }
}