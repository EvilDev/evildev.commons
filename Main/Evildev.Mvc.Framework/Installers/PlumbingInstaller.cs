﻿using System.Web.Http.Dependencies;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Evildev.Commons.Windsor;
using Evildev.Commons.Windsor.Mvc;
using Microsoft.Practices.ServiceLocation;
using WindsorDependencyResolver = Evildev.Commons.Windsor.Mvc.WebApi.WindsorDependencyResolver;

namespace Evildev.Mvc.Framework.Installers
{
    public class PlumbingInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IWindsorContainer>().Instance(container));
            container.Register(Component.For<IServiceLocator>().ImplementedBy<WindsorServiceLocator>());
            container.Register(Component.For<IDependencyResolver>().ImplementedBy<WindsorDependencyResolver>());
            container.Register(Component.For<IDependencyScope>().ImplementedBy<WindsorDependencyScope>().LifestyleTransient());
        }
    }
}