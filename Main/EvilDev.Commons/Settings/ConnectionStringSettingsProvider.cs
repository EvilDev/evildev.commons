﻿using System.Configuration;
using System.Web;
using System.Web.Configuration;

namespace EvilDev.Commons.Settings
{
    public class ConnectionStringSettingsProvider : IConnectionstringSettingsProvider
    {
        private readonly ConnectionStringSettingsCollection _connectionStringSettings;

        public ConnectionStringSettingsProvider()
        {
            _connectionStringSettings = HttpContext.Current == null ? ConfigurationManager.ConnectionStrings : WebConfigurationManager.ConnectionStrings;
        }

        public string this[string key]
        {
            get { return _connectionStringSettings[key].ConnectionString; }
            set { _connectionStringSettings[key].ConnectionString = value; }
        }

        public bool ContainsKey(string key)
        {
            return _connectionStringSettings[key] != null;
        }
    }
}