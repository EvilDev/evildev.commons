﻿namespace EvilDev.Commons.Settings
{
    public interface ISettingsProvider
    {
        string this[string key] { get; set; }

        bool ContainsKey(string key);
    }
}