﻿using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace EvilDev.Commons.Settings
{
    public class AppSettingsProvider : IAppSettingsProvider
    {
        private readonly NameValueCollection _appSettings;

        public AppSettingsProvider()
        {
            _appSettings = HttpContext.Current == null ? ConfigurationManager.AppSettings : WebConfigurationManager.AppSettings;
        }

        public string this[string key]
        {
            get { return _appSettings[key]; }
            set
            {
                _appSettings[key] = value;
            }
        }

        public bool ContainsKey(string key)
        {
            return _appSettings.AllKeys.AsEnumerable().Contains(key);
        }
    }
}