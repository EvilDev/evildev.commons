﻿using System;

namespace EvilDev.Commons
{
    public interface IBootstrapper : IDisposable
    {
        IBootstrapper Run();
    }
}