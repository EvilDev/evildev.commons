﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace EvilDev.Commons.EventBus
{
    public interface IPublisher
    {
        ISubscriber<TMessage> Subscribe<TMessage>(Action<TMessage> messageReciever);

        ISubscriber<TMessage> Subscribe<TMessage>();

        void Publish<TMessage>(TMessage message);

        void Unsubscribe<TMessage>(ISubscriber<TMessage> subscriber);
    }

    public class Publisher : IPublisher
    {
        private readonly IDictionary<Type, IList> _subscribers;

        public Publisher()
        {
            _subscribers = new Dictionary<Type, IList>();
        }

        public ISubscriber<TMessage> Subscribe<TMessage>(Action<TMessage> messageReciever)
        {
            var subscriber = new Subscriber<TMessage>(messageReciever, this);
            if (!_subscribers.ContainsKey(typeof (TMessage)))
            {
                
            }
        }

        public ISubscriber<TMessage> Subscribe<TMessage>()
        {
            throw new NotImplementedException();
        }

        public void Publish<TMessage>(TMessage message)
        {
            throw new NotImplementedException();
        }

        public void Unsubscribe<TMessage>(ISubscriber<TMessage> subscriber)
        {
            throw new NotImplementedException();
        }
    }
}