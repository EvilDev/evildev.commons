﻿using System;

namespace EvilDev.Commons.EventBus
{
    public interface ISubscriber<in TMessage> : IDisposable
    {
        Action<TMessage> Reciever { get; }
    }
}