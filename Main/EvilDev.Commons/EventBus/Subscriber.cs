﻿using System;

namespace EvilDev.Commons.EventBus
{
    public class Subscriber<TMessage> : ISubscriber<TMessage>
    {
        private readonly IPublisher _publisher;

        public Subscriber(Action<TMessage> reciever, IPublisher publisher)
        {
            Reciever = reciever;
            _publisher = publisher;
        }

        public void Dispose()
        {
            Disposing(true);
        }

        private void Disposing(bool isDisposing)
        {
            if (isDisposing)
            {
                _publisher.Unsubscribe(this);
            }
        }


        public Action<TMessage> Reciever { get; private set; }
    }
}