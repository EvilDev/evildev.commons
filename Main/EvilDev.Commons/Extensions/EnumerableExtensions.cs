﻿using System;
using System.Collections.Generic;

namespace EvilDev.Commons.Extensions
{
    public static class EnumerableExtensions
    {
         public static void ForEach<T>(this IEnumerable<T> enumerable, Action<int, T> action)
         {
             var i = 0;

             foreach (var item in enumerable)
             {
                 action(i, item);
                 i++;
             }
         }

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            enumerable.ForEach((i, item) => action(item));
        }
    }
}