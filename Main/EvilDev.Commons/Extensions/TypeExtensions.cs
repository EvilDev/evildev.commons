﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EvilDev.Commons.Extensions
{
    /// <summary>
    /// Provides a fluent api for querying types (useful for container registration by convention)
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Returns all types in the list that are assigable from <see cref="TInterface"/>
        /// </summary>
        /// <typeparam name="TInterface">Base Contract to filter by</typeparam>
        /// <param name="types">List of types to query</param>
        /// <returns>Filtered List of types</returns>
        public static IEnumerable<Type> BasedOn<TInterface>(this IEnumerable<Type> types)
        {
            return types.BasedOn(typeof (TInterface));
        }

        /// <summary>
        /// Returns all types in the list that are assigable from the specified contract
        /// </summary>
        /// <param name="contract">Base Contract to filter by</param>
        /// <param name="types">List of types to query</param>
        /// <returns>Filtered List of types</returns>
        public static IEnumerable<Type> BasedOn(this IEnumerable<Type> types, Type contract)
        {
            return types.Where(contract.IsAssignableFrom);
        }

        /// <summary>
        /// Picks the "best" interface to describe the type using the following rules:
        /// - if an interface exists in the same namespace/assembly and has the same name as the type's name, it will return that interface
        /// - else it will try to return the first interface if the type has one
        /// - lastly, it'll return the type if it implements no interfaces
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Type DefaultInterface(this Type type)
        {
            var namedInterface =
                type.GetInterfaces().FirstOrDefault(iface => iface.Name == string.Format("I{0}", type.Name));
            if (namedInterface != null)
            {
                return namedInterface;
            }

            if (type.FirstInterface() != null)
            {
                return type.FirstInterface();
            }

            return type;
        }

        public static IEnumerable<Type> Excluding(this IEnumerable<Type> types, params Type[] excludedTypes)
        {
            return types.Except(excludedTypes);
        }

        public static IEnumerable<Type> Excluding(this IEnumerable<Type> types, IEnumerable<Type> excludedTypes)
        {
            return types.Excluding(excludedTypes.ToArray());
        }

        /// <summary>
        /// Excludes <see cref="T"/> from the type list
        /// </summary>
        public static IEnumerable<Type> Excluding<T>(this IEnumerable<Type> types)
        {
            return types.Excluding(typeof (T));
        }

        /// <summary>
        /// Returns the first public interface the specified type implements
        /// </summary>
        /// <param name="type">type</param>
        /// <returns>First Public Interface implemented by type</returns>
        public static Type FirstInterface(this Type type)
        {
            return type.GetInterfaces().First(t => t.IsPublic);
        }

        public static IEnumerable<Type> Including(this IEnumerable<Type> types, params Type[] additionalTypes)
        {
            return types.Union(additionalTypes);
        }

        public static IEnumerable<Type> Including<T>(this IEnumerable<Type> types)
        {
            return types.Including(typeof (T));
        }

        /// <summary>
        /// Returns only types that implement an interface of the same name
        /// </summary>
        public static IEnumerable<Type> ThatImplementADefaultInterface(this IEnumerable<Type> types)
        {
            return
                types.Where(
                    type =>
                    type.GetInterfaces().FirstOrDefault(iface => iface.Name == string.Format("I{0}", type.Name)) != null);
        }

        public static IEnumerable<Type> WithNameEndingIn(this IEnumerable<Type> types, string suffix)
        {
            return types.Where(type => type.Name.EndsWith(suffix));
        }

        /// <summary>
        /// Returns all types in the same namespace as <see cref="T"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="types"></param>
        /// <returns></returns>
        public static IEnumerable<Type> WithSameNamespaceAs<T>(this IEnumerable<Type> types)
        {
            return types.Where(type => type.Namespace == typeof (T).Namespace);
        }

        /// <summary>
        /// Returns all types that have the same "root namespace" as <see cref="T"/>. Meaning all namespaces that start with the namespace of <see cref="T"/>
        /// <example>
        /// If <see cref="T"/>'s namespace was "System"
        /// All types in "System" and "System.Windows" would be returned for example
        /// </example>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="types"></param>
        /// <returns></returns>
        public static IEnumerable<Type> WithSameRootNamespaceAs<T>(this IEnumerable<Type> types)
        {
            var rootNamespace = typeof (T).Namespace ?? string.Empty;

            return types.Where(type => type.Namespace != null && type.Namespace.StartsWith(rootNamespace));
        }
    }
}