using System;

namespace EvilDev.Commons.Extensions
{
    public static class ComparableExtensions
    {
        public static bool IsBetween<T>(this T actual, T lower, T upper) where T : IComparable<T>
        {
            return actual.CompareTo(lower) >= 0 && actual.CompareTo(upper) < 0;
        }
    }
}