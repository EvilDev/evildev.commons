﻿using System;

namespace EvilDev.Commons.Extensions
{
    public static class TimespanExtensions
    {
        public static TimeSpan Hours(this int hours)
        {
            return new TimeSpan(hours, 0, 0);
        }

        public static TimeSpan Minutes(this int minutes)
        {
            return new TimeSpan(0, minutes, 0);
        }

        public static TimeSpan Days(this int days)
        {
            return new TimeSpan(days, 0, 0, 0);
        }

        public static TimeSpan Seconds(this int seconds)
        {
            return new TimeSpan(0, 0, seconds);
        }

        public static TimeSpan Weeks(this int weeks)
        {
            return new TimeSpan(weeks * 7, 0, 0, 0);
        }

        public static TimeSpan Milliseconds(this int milliseconds)
        {
            return new TimeSpan(0, 0, 0, 0, milliseconds);
        }

        public static double InDays(this TimeSpan span)
        {
            return span.TotalDays;
        }

        public static double InWeeks(this TimeSpan span)
        {
            return span.TotalDays / 7;
        }

        public static double InHours(this TimeSpan span)
        {
            return span.TotalHours;
        }

        public static double InMinutes(this TimeSpan span)
        {
            return span.TotalMinutes;
        }

        public static double InSeconds(this TimeSpan span)
        {
            return span.TotalSeconds;
        }

        public static double InMilliseconds(this TimeSpan span)
        {
            return span.TotalMilliseconds;
        }
    }
}