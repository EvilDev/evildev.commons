﻿using System;

namespace EvilDev.Commons.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
    public sealed class AssemblyLicenseUrlAttribute : Attribute
    {
        public AssemblyLicenseUrlAttribute(String licenseUrl)
        {
            LicenseUrl = licenseUrl;
        }

        public string LicenseUrl { get; set; }
    }
}