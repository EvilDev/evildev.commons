﻿using System;

namespace EvilDev.Commons.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
    public sealed class AssemblyIconUrlAttribute : Attribute
    {
        public AssemblyIconUrlAttribute(String iconUrl)
        {
            IconUrl = iconUrl;
        }

        public string IconUrl { get; set; }
    }
}
