﻿using System;

namespace EvilDev.Commons.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
    public sealed class AssemblyProjectUrlAttribute : Attribute
    {
        public AssemblyProjectUrlAttribute(String projectUrl)
        {
            ProjectUrl = projectUrl;
        }

        public string ProjectUrl { get; set; }
    }
}