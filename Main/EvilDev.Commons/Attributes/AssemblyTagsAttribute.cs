﻿using System;
using System.Collections.Generic;

namespace EvilDev.Commons.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true, Inherited = false)]
    public sealed class AssemblyTagsAttribute : Attribute
    {
        public IEnumerable<string> Tags { get; private set; }

        public AssemblyTagsAttribute(params string[] tags)
        {
            Tags = tags;
        }
    }
}