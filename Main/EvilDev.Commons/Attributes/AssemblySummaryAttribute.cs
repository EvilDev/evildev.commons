﻿using System;

namespace EvilDev.Commons.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
    public sealed class AssemblySummaryAttribute : Attribute
    {
        public AssemblySummaryAttribute(String summary)
        {
            Summary = summary;
        }

        public string Summary { get; set; }
    }
}