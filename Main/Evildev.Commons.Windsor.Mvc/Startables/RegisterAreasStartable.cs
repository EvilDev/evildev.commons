﻿using System.Web.Mvc;
using Castle.Core;

namespace Evildev.Commons.Windsor.Mvc.Startables
{
    public class RegisterAreasStartable : IStartable
    {
        public void Start()
        {
            AreaRegistration.RegisterAllAreas();
        }

        public void Stop()
        {
        }
    }
}