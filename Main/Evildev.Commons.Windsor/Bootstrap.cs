﻿using Castle.Windsor;
using EvilDev.Commons;

namespace Evildev.Commons.Windsor
{
    public static class Bootstrap
    {
         public static IBootstrapper WithWindsor()
         {
             return new WindsorBootstrapper(new WindsorContainer());
         }
    }
}