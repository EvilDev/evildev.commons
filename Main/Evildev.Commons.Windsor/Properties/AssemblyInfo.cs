﻿using System.Reflection;
using EvilDev.Commons.Attributes;

[assembly: AssemblyTitle("Evildev.Commons.Windsor")]
[assembly: AssemblyDescription("Handy framework, helpers and utilites for working with the Castle.Windsor Inversion of Control Container.")]
[assembly: AssemblySummary("Handy framework, helpers and utilites for working with the Castle.Windsor Inversion of Control Container.")]
[assembly: AssemblyCompany("Deville Solutions")]
[assembly: AssemblyProduct("Evildev.Commons.Windsor")]
[assembly: AssemblyCopyright("Copyright © Deville Solutions 2012")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyLicenseUrl("http://mit-license.org/")]
[assembly: AssemblyProjectUrl("http://code.evildev.net/evildev.commons/wiki/Home")]
[assembly: AssemblyIconUrl("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2012/Dec/06/evildev.commons-logo-3272278768-2_avatar.png")]