﻿using System;
using System.Reflection;
using Castle.Core;
using Castle.Facilities.Startable;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using EvilDev.Commons;
using EvilDev.Commons.Extensions;

namespace Evildev.Commons.Windsor
{
    public class WindsorBootstrapper : IBootstrapper
    {
        protected IWindsorContainer Container { get; private set; }

        public WindsorBootstrapper(IWindsorContainer container)
        {
            Container = container;
        }

        public void Dispose()
        {
            Container.Dispose();
        }

        public virtual IBootstrapper Run()
        {
            Container.AddFacility<StartableFacility>();
            Container.AddFacility<TypedFactoryFacility>();

            Container.Register(Component.For<IWindsorContainer>().Instance(Container));

            var entryPoint = Assembly.GetCallingAssembly();

            entryPoint.GetExportedTypes()
                    .BasedOn<IWindsorInstaller>()
                    .ForEach(installer => Container.Register(Component.For<IWindsorInstaller>()
                                                                       .Named(installer.FullName)
                                                                       .ImplementedBy(installer)));

            entryPoint.GetExportedTypes()
                    .BasedOn<IStartable>()
                    .ForEach(startable => Container.Register(Component.For<IStartable>().Named(startable.FullName).ImplementedBy(startable)));

            Container.Install(Container.ResolveAll<IWindsorInstaller>());

            return this;
        }
    }
}