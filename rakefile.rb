require "rubygems"
require 'bundler'
require 'fileutils'
require 'pathname'

system "bundle install --system"
Gem.clear_paths

require 'albacore'
require 'noodle'
require "rexml/document"

include REXML

Dir.glob('**/*.rake').each { |r| import r }

solution_file = FileList["**/*.sln"].first
ZIP_EXE = ENV['ZIP_EXE_PATH'] || '"C:\Program Files\7-Zip\7z.exe"'
NUGET = ENV['NUGET_EXE_PATH'] || FileList[".nuget/nuget.exe"].first

desc 'Builds the application'
task :default => ["build:packages"]

desc 'Builds the application'
task :build, [:config] => ["build:packages"]

namespace :build do
	task :solution, [:config] do |msb, args|
		supported_frameworks = ["v4.5","v4.0","v3.5"]

		supported_frameworks.each do |f| 
			spec = Rake::Task["build:framework"]
			spec.invoke(args[:config], f)
			spec.reenable()
		end
	end

	msbuild :framework, [:config, :framework] do |msb, args|
		msb.properties = {
			:configuration => args[:config] || :Debug,
			:platform => "Any CPU",
			:TargetframeworkVersion => args[:framework] || "v4.5",
			:OutputPath => "bin\\#{args[:config]}\\#{args[:framework]}"
		}

		msb.targets :Build
		msb.solution = solution_file
	end

	nugetpack :package, [:project, :config] do |nuget, args|
		config = args[:config] || "debug"
		project_file = FileList["Main/**/#{args[:project]}.csproj"].first
		project_dir = File.dirname(project_file)

		nuspec_file = FileList["#{project_dir}/*.nuspec"].first
		
		nuget.command = NUGET
		nuget.nuspec = nuspec_file
		nuget.base_folder = "."
		nuget.output = project_dir
		nuget.symbols = true
	end

	task :packages, [:config] => ["build:solution"] do |task, args|
		config = args[:config] || "debug"

		FileList["code/**/*.csproj"].each do |proj_file|
			proj = File.basename(proj_file, ".*")
			spec = Rake::Task["spec:project"]
			spec.invoke(proj, config)
			spec.reenable()

			package = Rake::Task["build:package"]
			package.invoke(proj, config)
			package.reenable()
		end
	end
end

namespace :spec do
	nuspec :project, [:project, :config, :framework] do |nuspec, args|
		config = args[:config] || "debug"
		project_file = FileList["Main/**/#{args[:project]}.csproj"].first
		project_dir = File.dirname(project_file)
		puts project_dir
		package_id = File.basename(project_file, ".*")
		package_dir = File.expand_path(Dir["Main/**/*#{args[:project]}*/bin/#{config}"].first)
		package_config = FileList["#{project_dir}/packages.config"].first

		assembly_file = FileList["#{project_dir}/bin/#{config}/*#{package_id}.dll"].first.gsub("/","\\")
		pdb_file = FileList["#{project_dir}/bin/#{config}/*#{package_id}.pdb"].first.gsub("/","\\")

		project_xml = File.read(project_file)
		project_doc = REXML::Document.new(project_xml)

		nuspec.id = package_id
		nuspec.version = ENV['BUILD_NUMBER'] || "1.0.0"
		nuspec.authors = "Mitchell Lee"
		nuspec.description = ""
		nuspec.title = package_id
		nuspec.language = "en-US"
		nuspec.projectUrl = "http://code.evildev.net"
		nuspec.file "#{project_dir}\\bin\\#{args[:config]}\\v3.5\\*.dll", "lib\\net35"
		nuspec.file "#{project_dir}\\bin\\#{args[:config]}\\v4.0\\*.dll", "lib\\net40"
		nuspec.file "#{project_dir}\\bin\\#{args[:config]}\\v4.5\\*.dll", "lib\\net45"
		nuspec.working_directory = project_dir
		nuspec.output_file = "#{package_id}.nuspec"

		if File.exist? package_config
			packages_xml = File.read(package_config)
			packages_doc = REXML::Document.new(packages_xml)
			packages_doc.elements.each("packages/package") do |package|
				nuspec.dependency package.attributes["id"], package.attributes["version"]
			end
		end

		project_doc.elements.each("Project/ItemGroup/ProjectReference/Name") do |proj|
			nuspec.dependency proj.text, (ENV['BUILD_NUMBER'] || "1.0.0")
		end
	end
end